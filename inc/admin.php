<?php
/**
 * Created by PhpStorm.
 * User: jamescallahan
 * Date: 5/31/17
 * Time: 8:26 AM
 */

// Hook functions to actions
add_action( 'admin_enqueue_scripts', 'fb_jbc_admin_enqueue' );
add_action( 'add_meta_boxes', 'fb_jbc_custom_metabox' );

/**
 * Enqueue Styles and Scripts
 */
function fb_jbc_admin_enqueue() {
	wp_enqueue_style( 'fs-admin-style', plugin_dir_url( __FILE__ ) . 'css/admin-style.css' );
}

/**
 * Create Custom Metaboxes
 */
function fb_jbc_custom_metabox() {
	add_meta_box( 'feat-book', 'Featured Books', 'fb_jbc_display_books', 'post', 'side' );
}

/**
 * Display Checklist for Featured Books
 */
function fb_jbc_display_books() {

	$feat_items = get_post_meta( get_the_ID(), 'featured-books', true );

	$allbooks = get_posts(
		array(
			'post_type' => 'book',
			'numberposts' => -1,
		)
	);

	foreach ( $allbooks as $book ) :
		setup_postdata( $book );
	?>

	<div class="featured-book">
		<label for="featured-book">
			<input type="checkbox" name="featured-books[]" value="<?php echo $book->ID; ?>"
																				<?php
																				foreach ( $feat_items as $item ) {
																					checked( $item, $book->ID );
																				}
			?>
			><?php echo $book->post_title; ?>
		</label>
	</div>

	<?php
	endforeach;

	wp_reset_postdata();

}

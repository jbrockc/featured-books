<?php
/*
Plugin Name: Featured Books
Description: Displays featured books at the bottom of posts
Author: Brock Callahan
Version: 0.1.1
*/

add_action( 'wp_enqueue_scripts', 'fb_jbc_frontend_enqueue' );

if ( is_admin() ) {
	include_once 'inc/admin.php';
}

/**
 * Enqueue styles and scripts
 */
function fb_jbc_frontend_enqueue() {
	wp_enqueue_style( 'fs-styles', plugin_dir_url( __FILE__ ) . 'css/style.css' );
}

/**
 * Show Featured Stuff on single posts
 */
add_filter( 'the_content', 'fb_jbc_show_featured_books' );
function fb_jbc_show_featured_books( $content ) {

	if ( ! is_singular( 'post' ) ) {
		return $content;
	}

	$feat = get_post_meta( get_the_ID(), 'featured-books', true );

	// If there are no featured items, quit
	if ( ! is_array( $feat ) || ! count( $feat ) ) {
		return $content;
	}

	$args = array(
		'post_type' => 'book',
		'post__in' => $feat,
	);

	$query = new WP_Query( $args );

	$featured_stuff = "<div class='featured-book-wrap'><header><h2>Featured Books</h2><p>This article contains extracts from the following books:</p></header>";

	$featured_stuff .= "<article class='featured-book-body'>"; // Open wrap div

	if ( $query->have_posts() ) :
		while ( $query->have_posts() ) :
			$query->the_post();

			$featured_stuff .= '<div class="featured-book-item"><a href=' . get_the_permalink() . '>';
			$featured_stuff .= get_the_post_thumbnail();
			$featured_stuff .= '</a></div>';

	endwhile;
	endif;

	$featured_stuff .= '<sub>Click on the covers for more details.</sub>';
	$featured_stuff .= '</article></div>'; // Close wrap div

	wp_reset_postdata();

	$content .= $featured_stuff;

	return $content;
}

/**
 * Save Featured Books Data on Single Posts
 */
add_action( 'save_post', 'fb_jbc_save_feat_books_as_post_meta_data' );
function fb_jbc_save_feat_books_as_post_meta_data( $post_id ) {

	if ( isset( $_POST['featured-books'] ) ) {
		update_post_meta( $post_id, 'featured-books', $_POST['featured-books'] );
	} else {
		delete_post_meta( $post_id, 'featured-books' );
	}

}
